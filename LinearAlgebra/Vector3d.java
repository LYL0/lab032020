/*Louisa Yuxin Lin 1933472*/
package LinearAlgebra;
      
 public class Vector3d {
      
 private double x;
 private double y;
 private double z;
       
 public Vector3d(double num1, double num2, double num3) { 
       
     this.x = num1;
   this.y = num2; 
   this.z = num3; 
 }
        
 public double getX() {
  return this.x;
 }
 
 public double getY() {
  return this.y;
 }
 
 public double getZ() {
  return this.z;
 }
       
 public double magnitude() {
  double m = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
  return m;
 }
 
 public double dotProduct(Vector3d v) {
  double product = (this.x * v.x) + (this.y * v.y) + (this.z * v.z);
  return product;
 }
       
 public Vector3d add(Vector3d v) {
  Vector3d newVector = new Vector3d(this.x + v.x , this.y + v.y , this.z + v.z);
  return newVector;
 }
}
