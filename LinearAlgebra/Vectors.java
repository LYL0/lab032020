/*Louisa Yuxin Lin 1933472*/
package LinearAlgebra;

public class Vectors {
  public static void main(String[] args) {
  
   Vector3d vector = new Vector3d(1,2,3);
  
   System.out.print(vector.magnitude());
   
   Vector3d vector2 = new Vector3d(4,5,6);
   
   System.out.print(vector.dotProduct(vector2));
   
   System.out.print(vector.add(vector2));
 }

}
